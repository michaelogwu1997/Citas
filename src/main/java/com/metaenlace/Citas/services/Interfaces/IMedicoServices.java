package com.metaenlace.Citas.services.Interfaces;

import com.metaenlace.Citas.models.Medico;
import com.metaenlace.Citas.models.Paciente;

import java.util.List;
import java.util.Optional;

public interface IMedicoServices {
    public List<Medico> findAll();
    public Medico add(Medico p);
    public boolean delete(int id);
    public Optional<Medico> find (int id);
}
