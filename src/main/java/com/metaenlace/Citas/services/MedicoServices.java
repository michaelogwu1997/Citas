package com.metaenlace.Citas.services.Interfaces;

import com.metaenlace.Citas.models.Medico;
import com.metaenlace.Citas.models.Paciente;
import com.metaenlace.Citas.repositories.MedicoRepository;
import com.metaenlace.Citas.repositories.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class MedicoServices implements  IMedicoServices {
    @Autowired
    MedicoRepository mr;
    @Override
    public List<Medico> findAll(){
        return mr.findAll();
    }
    @Override
    public Medico add(Medico m){
        return mr.save(m);
    }
    @Override
    public boolean delete(int id){
        try{
            mr.deleteById(id);
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
    @Override
    public Optional<Medico> find (int id){
        return mr.findById(id);
    }
}
