package com.metaenlace.Citas.controllers;

import com.metaenlace.Citas.models.DTO.MedicoDTO;
import com.metaenlace.Citas.models.Medico;
import com.metaenlace.Citas.services.MedicoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.modelmapper.ModelMapper;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/medico")
@CrossOrigin(origins = "http://localhost:4200/")
public class MedicoController {
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    MedicoServices ms;
    @GetMapping("mostrar")
    public List<MedicoDTO> findAll(){
        return  ms.findAll().stream().map(post -> modelMapper.map(post,MedicoDTO.class))
                .collect(Collectors.toList());
    }
    @GetMapping("mostrarPorId/{id}")
    public Optional<Medico> find(@PathVariable("id") int id){
        return ms.find(id);
    }
    @DeleteMapping("borrarPorId/{id}")
    public boolean delete( @PathVariable("id") int id){
        return ms.delete(id);
    }
    @PostMapping("introducir")
    public MedicoDTO add(@RequestBody MedicoDTO mDTO){
        Medico m= modelMapper.map(mDTO,Medico.class);
        Medico mPost= ms.add(m);
        mDTO=modelMapper.map(mPost,MedicoDTO.class);
        return mDTO;
    }
}
