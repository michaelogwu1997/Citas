package com.metaenlace.Citas.controllers;

import com.metaenlace.Citas.models.DTO.PacienteDTO;
import com.metaenlace.Citas.models.Paciente;
import org.modelmapper.ModelMapper;
import com.metaenlace.Citas.models.Usuario;
import com.metaenlace.Citas.repositories.PacienteRepository;
import com.metaenlace.Citas.services.PacienteServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/paciente")
@CrossOrigin(origins = "http://localhost:4200/")
public class PacienteController {
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    PacienteServices ps;
    @GetMapping("mostrar")
    public List<PacienteDTO> findAll(){
        return  ps.findAll().stream().map(post -> modelMapper.map(post, PacienteDTO.class))
                .collect(Collectors.toList());
    }
    @GetMapping("mostrarPorId/{id}")
    public Optional<Paciente> find(@PathVariable("id") int id){
        return ps.find(id);
    }
    @DeleteMapping("borrarPorId/{id}")
    public boolean delete( @PathVariable("id") int id){
        return ps.delete(id);
    }
    @PostMapping("introducir")
    public PacienteDTO add(@RequestBody PacienteDTO pDto){
        Paciente p=modelMapper.map(pDto,Paciente.class);
        Paciente pPost=ps.add(p);
        pDto=modelMapper.map(pPost,PacienteDTO.class);
        return pDto;
    }

}
