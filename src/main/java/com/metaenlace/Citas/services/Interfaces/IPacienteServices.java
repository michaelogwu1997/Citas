package com.metaenlace.Citas.services.Interfaces;

import com.metaenlace.Citas.models.Paciente;

import java.util.List;
import java.util.Optional;

public interface IPacienteServices {
    public List<Paciente> findAll();
    public Paciente add(Paciente p);
    public boolean delete(int id);
    public Optional<Paciente> find (int id);
}
